rm -rf /opt/ANDRAX/birp

source /opt/ANDRAX/PYENV/python2/bin/activate

/opt/ANDRAX/PYENV/python2/bin/pip2 install six colorama IPython

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install... PASS!"
else
  # houston we have a problem
  exit 1
fi

mkdir /opt/ANDRAX/birp

cp -Rf birp.py getch.py py3270andrax py3270wrapper.py suite3270-full.patch tn3270.py /opt/ANDRAX/birp/

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/
chmod -R 755 /opt/ANDRAX/
